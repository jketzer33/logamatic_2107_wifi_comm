# logamatic_2107_wifi_comm

A direct KM217 replacement with WiFi connection to control a buderus heating system with Logamatic 2107 controller board.

For more details, look at my [blog post](https://the78mole.de/reverse-engineering-the-buderus-km217/) or at [tindie](https://www.tindie.com/products/the78mole/buderus-km217-wifi-replacement/).

Here is a Version 0.0.1 board with fixes (corrected RX & TX pins of ESP32):

![v0.0.1_fixed](IMG/Buderus-KM217-Clone_0.0.1_with_fixes.jpg)